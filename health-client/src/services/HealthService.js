import axios from 'axios';

const BASE_URL = "/api";
const ING_BASE_URL = '/api/ingredient';
const RECIPE_BASE_URL = '/api/recipe';
const USER_FOOD_URL = "/api/userFoodItem";

export default class HealthService {
    static async createIngredient(ingredient) {
        return axios.post(ING_BASE_URL + '/create', ingredient);
    }

    static async getIngredients() {
        return axios.get(ING_BASE_URL);
    }

    static async createRecipe(recipe) {
        return axios.post(RECIPE_BASE_URL + '/create', recipe);
    }

    static async getRecipes() {
        return axios.get(RECIPE_BASE_URL);
    }

    static async addUserFoodItem(foodItem) {
        return axios.post(USER_FOOD_URL + '/create', foodItem);
    }

    static async getUserFoodItems(date) {
        return axios.get(`${USER_FOOD_URL}?date=${date}`);
    }

    static async login(form) {
        return axios.post(BASE_URL + "/login", form);
    }

    static async register(form) {
        return axios.post(BASE_URL + "/register", form);
    }

    static async isLoggedIn() {
        return axios.get(BASE_URL + "/isLoggedIn");
    }
} 