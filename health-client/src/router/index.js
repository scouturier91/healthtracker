import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../pages/Home';
import AddIngredient from '../pages/AddIngredient';
import AddRecipe from '../pages/AddRecipe';
import Login from '../pages/Login';
import Register from '../pages/Register';

Vue.use(VueRouter);

export default new VueRouter({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/addIngredient',
      name: 'addIngredient',
      component: AddIngredient
    },
    {
      path: '/addRecipe',
      name: 'addRecipe',
      component: AddRecipe
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    }
  ]
});