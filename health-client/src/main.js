import router from './router/index';
import App from './App';
import Vue from 'vue';
import { BootstrapVue } from 'bootstrap-vue';
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import Toasted from 'vue-toasted';
 
Vue.use(Toasted)
Vue.use(BootstrapVue);

new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components:{ App }
});
