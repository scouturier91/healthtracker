package com.healthtracker.webapp.security;

import com.healthtracker.webapp.domain.HealthUser;
import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;


/**
 * Object to store the users session info.
 */
@Data
@Component
@Scope(value= WebApplicationContext.SCOPE_SESSION, proxyMode= ScopedProxyMode.TARGET_CLASS)
public class SessionInfo {
    private HealthUser user;
}
