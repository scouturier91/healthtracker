package com.healthtracker.webapp.domain;

import com.healthtracker.webapp.domain.key.RecipeIngredientId;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@NoArgsConstructor
@Data
@IdClass(RecipeIngredientId.class)
@Table(name = "RECIPE_INGREDIENTS")
public class RecipeIngredients implements Serializable {
    @Id
    private Integer recipeId;

    @Id
    private Integer ingredientId;
}
