package com.healthtracker.webapp.domain;

import com.healthtracker.webapp.dto.UserFoodItemDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Data
@NoArgsConstructor
@Table(name = "USER_FOOD")
public class UserFoodItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="USER_FOOD_ID")
    private Integer id;

    private Integer userId;

    private Long recipeId;

    private Date foodDate;

    private Float servingSize;

    private Character mealType;

    private Integer calories;

    private String name;

    private Integer protein;

    private Integer fat;

    private Integer carbs;

    public UserFoodItem(UserFoodItemDto dto) {
        this.id = dto.getId();
        this.userId = dto.getUserId();
        this.recipeId = dto.getRecipeId();
        this.foodDate = dto.getFoodDate() == null ? new Date(System.currentTimeMillis()) : dto.getFoodDate();
        this.servingSize = dto.getServingSize();
        this.mealType = dto.getMealType();
        this.name = dto.getName();
        this.calories = dto.getCalories();
        this.protein = dto.getProtein();
        this.fat = dto.getFat();
        this.carbs = dto.getCarbs();
    }
}
