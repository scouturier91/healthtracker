package com.healthtracker.webapp.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data
@Table(name = "HEALTH_USER")
public class HealthUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer healthUserId;

    private String username;

    @Column(name = "USER_PASS")
    private String password;
}
