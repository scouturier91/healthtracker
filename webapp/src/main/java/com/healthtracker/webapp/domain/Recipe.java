package com.healthtracker.webapp.domain;

import com.healthtracker.webapp.dto.RecipeDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data
@Table(name = "RECIPE")
public class Recipe {
    @Id
    @Column(name="RECIPE_ID", columnDefinition = "SERIAL")
    private Integer recipeId;

    @Column(name="NAME")
    private String name;

    @Column
    private Integer calories;

    @Column
    private Integer protein;

    @Column
    private Integer fat;

    @Column
    private Integer carbs;

    @Column
    private Double servingSize;

    @Column
    private String unit;

    public Recipe(RecipeDto recipeDto) {
        this.name = recipeDto.getName();
        this.servingSize = recipeDto.getSize();
        this.unit = recipeDto.getUnit();

        this.calories = 0;
        this.protein = 0;
        this.fat = 0;
        this.carbs = 0;

        if (recipeDto.getIngredients() != null) {
            recipeDto.getIngredients().forEach(val -> {
                this.calories += val.getCalories() == null ? 0 : val.getCalories();
                this.protein += val.getProtein() == null ? 0 : val.getProtein();
                this.fat += val.getFat() == null ? 0 : val.getProtein();
                this.carbs += val.getCarbs() == null ? 0 : val.getCarbs();
            });
        }
    }
}
