package com.healthtracker.webapp.domain;

import com.healthtracker.webapp.dto.IngredientDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data
@Table(name = "INGREDIENT")
public class Ingredient {
    @Id
    @Column(name = "INGREDIENT_ID", columnDefinition = "SERIAL")
    private Integer ingredientId;

    @Column
    private Integer calories;

    @Column
    private String name;

    @Column
    private Integer protein;

    @Column
    private Integer fat;

    @Column
    private Integer carbs;

    @Column
    private Integer servingSize;

    @Column
    private String unit;

    public Ingredient(IngredientDto dto) {
        this.ingredientId = 1;
        this.calories = dto.getCalories();
        this.name = dto.getName();
        this.protein = dto.getProtein();
        this.fat = dto.getFat();
        this.carbs = dto.getCarbs();
        this.servingSize = dto.getServingSize();
        this.unit = dto.getUnit();
    }
}
