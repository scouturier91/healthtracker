package com.healthtracker.webapp.controller;

import com.healthtracker.webapp.dto.UserFoodItemDto;
import com.healthtracker.webapp.dto.UserNutritionDto;
import com.healthtracker.webapp.security.SessionInfo;
import com.healthtracker.webapp.service.UserFoodItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/userFoodItem")
@Scope("request")
public class UserFoodItemController {
    @Autowired
    private SessionInfo sessionInfo;

    @Autowired
    private UserFoodItemService userFoodItemService;

    @GetMapping("")
    UserNutritionDto getUserFoodItemsForDate(@RequestParam("date") String date) {
        return userFoodItemService.getUserNutrition(date, sessionInfo.getUser().getHealthUserId());
    }

    @PostMapping("/create")
    Integer addUserFoodItem(@RequestBody UserFoodItemDto userFoodItemDto) {
        return userFoodItemService.createUserFoodItem(userFoodItemDto, sessionInfo.getUser().getHealthUserId());
    }
}
