package com.healthtracker.webapp.controller;

import com.healthtracker.webapp.domain.Ingredient;
import com.healthtracker.webapp.dto.IngredientDto;
import com.healthtracker.webapp.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/ingredient")
public class IngredientController {
    @Autowired
    IngredientService ingredientService;

    @PostMapping("create")
    ResponseEntity<Integer> createIngredient(@RequestBody IngredientDto ingredient) {
        return ResponseEntity.ok(ingredientService.createIngredient(ingredient));
    }

    @GetMapping("")
    ResponseEntity<List<Ingredient>> getIngredients() {
        return ResponseEntity.ok(ingredientService.getIngredients());
    }
}
