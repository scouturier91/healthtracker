package com.healthtracker.webapp.controller;

import com.healthtracker.webapp.domain.HealthUser;
import com.healthtracker.webapp.dto.HealthUserDto;
import com.healthtracker.webapp.security.SessionInfo;
import com.healthtracker.webapp.service.HealthUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@SessionAttributes("sessionInfo")
@RequestMapping("/api")
@Scope("request")
public class MainController {

    @Autowired
    private SessionInfo sessionInfo;

    @Autowired
    HealthUserService healthUserService;

    @GetMapping("isLoggedIn")
    ResponseEntity<Boolean> isLoggedIn() {
        return ResponseEntity.ok(sessionInfo.getUser() != null);
    }

    @PostMapping("login")
    ResponseEntity<Boolean> login(@RequestBody HealthUserDto healthUser) {
        HealthUser user = healthUserService.login(healthUser.getUsername(), healthUser.getPassword());
        sessionInfo.setUser(user);
        return ResponseEntity.ok(user != null);
    }

    @PostMapping("register")
    ResponseEntity<Boolean> register(@RequestBody HealthUserDto healthUser) {
        HealthUser user = healthUserService.register(healthUser.getUsername(), healthUser.getPassword());
        sessionInfo.setUser(user);
        return ResponseEntity.ok(user!= null);
    }
}
