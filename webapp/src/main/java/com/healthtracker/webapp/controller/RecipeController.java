package com.healthtracker.webapp.controller;

import com.healthtracker.webapp.domain.Recipe;
import com.healthtracker.webapp.dto.RecipeDto;
import com.healthtracker.webapp.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/recipe")
public class RecipeController {

    @Autowired
    RecipeService recipeService;

    @GetMapping("")
    List<Recipe> getRecipes() {
        return recipeService.getRecipes();
    }

    @PostMapping("create")
    ResponseEntity<Integer> createRecipe(@RequestBody RecipeDto recipe) {
        return ResponseEntity.ok(recipeService.createRecipe(recipe));
    }
}
