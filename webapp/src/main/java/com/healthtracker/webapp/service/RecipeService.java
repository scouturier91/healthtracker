package com.healthtracker.webapp.service;

import com.healthtracker.webapp.domain.Recipe;
import com.healthtracker.webapp.dto.RecipeDto;

import java.util.List;

public interface RecipeService {
    Integer createRecipe(RecipeDto recipeDto);

    List<Recipe> getRecipes();
}
