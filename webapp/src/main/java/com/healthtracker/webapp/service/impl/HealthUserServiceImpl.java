package com.healthtracker.webapp.service.impl;

import com.healthtracker.webapp.domain.HealthUser;
import com.healthtracker.webapp.repository.HealthUserRepository;
import com.healthtracker.webapp.security.SessionInfo;
import com.healthtracker.webapp.service.HealthUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;

@Service
public class HealthUserServiceImpl implements HealthUserService {

    @Autowired
    private HealthUserRepository healthUserRepository;

    @Override
    public HealthUser login(String name, String password) {
        // Get the user to check the credentials.
        HealthUser user = healthUserRepository.findAll().stream().filter(val -> val.getUsername().toLowerCase()
                .equals(name.toLowerCase())).findFirst().orElse(null);
        if (user != null) {
            if (password.equals(user.getPassword())) {
                return user;
            }
        }
        return null;
    }

    @Override
    public HealthUser register(String name, String password) {
        // Make sure the user isnt already logged in.
        HealthUser user = healthUserRepository.findAll().stream().filter(val -> val.getUsername().toLowerCase()
                .equals(name.toLowerCase())).findFirst().orElse(null);
        if (user == null) {
            // Create record
            user = new HealthUser();
            user.setUsername(name);
            user.setPassword(password);

            return healthUserRepository.save(user);
        }
        return null;
    }
}

