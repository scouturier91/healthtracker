package com.healthtracker.webapp.service.impl;

import com.healthtracker.webapp.domain.Ingredient;
import com.healthtracker.webapp.dto.IngredientDto;
import com.healthtracker.webapp.repository.IngredientRepository;
import com.healthtracker.webapp.service.IngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientServiceImpl implements IngredientService {
    @Autowired
    private IngredientRepository ingredientRepository;

    @Override
    public Integer createIngredient(IngredientDto ingredient) {
        // Get the latest id to set for the record.
        Integer latestId =  ingredientRepository.getLatestId();
        Integer newId = (latestId == null ? 1 : latestId) + 1;

        Ingredient toSave = new Ingredient(ingredient);
        toSave.setIngredientId(newId);

        Ingredient newIng = ingredientRepository.save(toSave);
        return newIng.getIngredientId();
    }

    @Override
    public List<Ingredient> getIngredients() {
        return ingredientRepository.findAll();
    }
}
