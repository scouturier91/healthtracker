package com.healthtracker.webapp.service.impl;

import com.healthtracker.webapp.domain.RecipeIngredients;
import com.healthtracker.webapp.dto.IngredientDto;
import com.healthtracker.webapp.repository.RecipeIngredientRepository;
import com.healthtracker.webapp.service.RecipeIngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RecipeIngredientServiceImpl implements RecipeIngredientService {
    @Autowired
    RecipeIngredientRepository recipeIngredientRepository;

    @Override
    public void addAllRecipeIngredients(Integer recipeId, List<IngredientDto> ings) {
        List<RecipeIngredients> recIngs = new ArrayList<>();

        // Create a relation entry for each ingredient in the recipe.
        ings.stream().forEach(ing -> {
            RecipeIngredients recIng = new RecipeIngredients();
            recIng.setIngredientId(ing.getIngredientId());
            recIng.setRecipeId(recipeId);
            recIngs.add(recIng);
        });

        recipeIngredientRepository.saveAll(recIngs);
    }
}
