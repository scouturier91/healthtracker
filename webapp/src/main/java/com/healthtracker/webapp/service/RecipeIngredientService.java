package com.healthtracker.webapp.service;

import com.healthtracker.webapp.dto.IngredientDto;

import java.util.List;

public interface RecipeIngredientService {
    void addAllRecipeIngredients(Integer recipeId, List<IngredientDto> ings);
}
