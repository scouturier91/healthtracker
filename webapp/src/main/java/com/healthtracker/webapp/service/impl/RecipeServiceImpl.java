package com.healthtracker.webapp.service.impl;

import com.healthtracker.webapp.domain.Recipe;
import com.healthtracker.webapp.dto.RecipeDto;
import com.healthtracker.webapp.repository.RecipeRepository;
import com.healthtracker.webapp.service.RecipeIngredientService;
import com.healthtracker.webapp.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecipeServiceImpl implements RecipeService {
    @Autowired
    RecipeRepository recipeRepository;

    @Autowired
    RecipeIngredientService recipeIngredientService;

    @Override
    public Integer createRecipe(RecipeDto recipeDto) {
        // Get the latest id to set for the new record.
        Integer latestId = recipeRepository.getLatestId();
        latestId = latestId == null ?  1 : latestId + 1;
        Recipe recipe = new Recipe(recipeDto);
        recipe.setRecipeId(latestId);

        Integer recId =  recipeRepository.save(recipe).getRecipeId();

        recipeIngredientService.addAllRecipeIngredients(recId, recipeDto.getIngredients());

        return recId;
    }

    @Override
    public List<Recipe> getRecipes() {
        return recipeRepository.findAll();
    }
}
