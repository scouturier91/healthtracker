package com.healthtracker.webapp.service;

import com.healthtracker.webapp.domain.Ingredient;
import com.healthtracker.webapp.dto.IngredientDto;

import java.util.List;

public interface IngredientService {
    Integer createIngredient(IngredientDto ingredient);

    List<Ingredient> getIngredients();
}
