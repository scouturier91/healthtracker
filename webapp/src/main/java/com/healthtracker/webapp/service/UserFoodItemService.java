package com.healthtracker.webapp.service;

import com.healthtracker.webapp.dto.UserFoodItemDto;
import com.healthtracker.webapp.dto.UserNutritionDto;

public interface UserFoodItemService {
    Integer createUserFoodItem(UserFoodItemDto userFoodItem, Integer userId);

    UserNutritionDto getUserNutrition(String dateString, Integer userId);
}
