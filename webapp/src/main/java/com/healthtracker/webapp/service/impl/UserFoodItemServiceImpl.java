package com.healthtracker.webapp.service.impl;

import com.healthtracker.webapp.domain.UserFoodItem;
import com.healthtracker.webapp.dto.UserFoodItemDto;
import com.healthtracker.webapp.dto.UserNutritionDto;
import com.healthtracker.webapp.repository.UserFoodItemRepository;
import com.healthtracker.webapp.security.SessionInfo;
import com.healthtracker.webapp.service.UserFoodItemService;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserFoodItemServiceImpl implements UserFoodItemService {
    @Autowired
    UserFoodItemRepository userFoodItemRepository;

    @Override
    public Integer createUserFoodItem(UserFoodItemDto userFoodItemDto, Integer userId) {
        // Get the latest id to set.
        Integer latestId = userFoodItemRepository.getLatestId();
        latestId = latestId == null ? 1 : latestId + 1;
        userFoodItemDto.setId(latestId);
        userFoodItemDto.setUserId(userId);

        return userFoodItemRepository.save(new UserFoodItem(userFoodItemDto)).getId();
    }

    @Override
    public UserNutritionDto getUserNutrition(String dateString, Integer userId) {
        UserNutritionDto nutritionDto = new UserNutritionDto();

        List<UserFoodItem> items = userFoodItemRepository.findAll().stream().filter(val -> val.getUserId().equals(userId))
                .collect(Collectors.toList());
        nutritionDto.addFoodItems(items);

        // Add the ingredients nutrition to the recipe.
        items.forEach(val -> {
            Integer cals = val.getCalories();
            Integer protein = val.getProtein();
            Integer fat = val.getFat();
            Integer carbs = val.getCarbs();
            if (cals != null) {
                nutritionDto.setTotalCalories(nutritionDto.getTotalCalories() + cals);
            }
            if (protein != null) {
                nutritionDto.setTotalProtein(nutritionDto.getTotalProtein() + protein);
            }
            if (fat != null) {
                nutritionDto.setTotalFat(nutritionDto.getTotalFat() + fat);
            }
            if (carbs != null) {
                nutritionDto.setTotalCarbs(nutritionDto.getTotalCarbs() + carbs);
            }
        });

        return nutritionDto;
    }
}
