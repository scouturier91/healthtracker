package com.healthtracker.webapp.service;

import com.healthtracker.webapp.domain.HealthUser;

public interface HealthUserService {

    HealthUser login(String name, String password);

    HealthUser register(String name, String password);
}
