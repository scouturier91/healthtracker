package com.healthtracker.webapp.dto;

import lombok.Data;

@Data
public class HealthUserDto {
    private String username;

    private String password;
}
