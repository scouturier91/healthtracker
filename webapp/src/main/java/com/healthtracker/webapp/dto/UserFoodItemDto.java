package com.healthtracker.webapp.dto;

import lombok.Data;

import java.sql.Date;

@Data
public class UserFoodItemDto {
    private Integer id;

    private Integer userId;

    private Long recipeId;

    private Date foodDate;

    private Float servingSize;

    private Character mealType;

    private Integer calories;

    private String name;

    private Integer protein;

    private Integer fat;

    private Integer carbs;

    private String unit;
}
