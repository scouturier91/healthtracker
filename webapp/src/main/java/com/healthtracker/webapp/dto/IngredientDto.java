package com.healthtracker.webapp.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class IngredientDto {

    private Integer ingredientId;

    private Integer calories;

    private String name;

    private Integer protein;

    private Integer fat;

    private Integer carbs;

    private Integer servingSize;

    private String unit;
}
