package com.healthtracker.webapp.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class RecipeDto {
    private String name;

    private List<IngredientDto> ingredients;

    private Double size;

    private String unit;
}
