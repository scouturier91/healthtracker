package com.healthtracker.webapp.dto;

import com.healthtracker.webapp.domain.UserFoodItem;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserNutritionDto {
    private int totalCalories = 0;

    private int totalProtein = 0;

    private int totalFat = 0;

    private int totalCarbs = 0;

    private List<UserFoodItem> breakfast = new ArrayList<>();

    private List<UserFoodItem> lunch = new ArrayList<>();

    private List<UserFoodItem> dinner = new ArrayList<>();

    private List<UserFoodItem> snacks = new ArrayList<>();

    public void addFoodItems(List<UserFoodItem> items) {
        items.forEach(val -> {
            switch (val.getMealType()) {
                case 'B':
                    this.breakfast.add(val);
                    break;
                case 'L':
                    this.lunch.add(val);
                    break;
                case 'D':
                    this.dinner.add(val);
                    break;
                case 'S':
                    this.snacks.add(val);
                    break;
                default:
                    break;
            }

//            this.totalCalories += val.
        });
    }
}
