package com.healthtracker.webapp.repository.custom;

import com.healthtracker.webapp.domain.UserFoodItem;

import java.util.List;

public interface UserFoodItemRepositoryCustom {

    Integer getLatestId();

    List<UserFoodItem> getUserFoodForDate(String dateString);
}
