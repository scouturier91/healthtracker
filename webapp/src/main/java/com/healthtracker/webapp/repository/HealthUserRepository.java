package com.healthtracker.webapp.repository;

import com.healthtracker.webapp.domain.HealthUser;
import com.healthtracker.webapp.repository.custom.HealthUserRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HealthUserRepository extends JpaRepository<HealthUser, Long>, HealthUserRepositoryCustom {
}
