package com.healthtracker.webapp.repository.impl;

import com.healthtracker.webapp.domain.UserFoodItem;
import com.healthtracker.webapp.repository.custom.UserFoodItemRepositoryCustom;
import com.healthtracker.webapp.security.SessionInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class UserFoodItemRepositoryImpl implements UserFoodItemRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    @Autowired
    private SessionInfo sessionInfo;

    @Override
    public Integer getLatestId() {
        String query = "SELECT MAX(USER_FOOD_ID) FROM USER_FOOD";

        return (Integer) em.createNativeQuery(query).getSingleResult();
    }

    @Override
    public List<UserFoodItem> getUserFoodForDate(String dateString) {
        String query = "SELECT * FROM USER_FOOD WHERE FOOD_DATE = '" + dateString + "' AND USER_ID = "
                + sessionInfo.getUser().getHealthUserId();

        return em.createNativeQuery(query).getResultList();
    }
}
