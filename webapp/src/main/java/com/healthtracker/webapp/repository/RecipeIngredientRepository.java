package com.healthtracker.webapp.repository;

import com.healthtracker.webapp.domain.RecipeIngredients;
import com.healthtracker.webapp.repository.custom.RecipeIngredientRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecipeIngredientRepository extends JpaRepository<RecipeIngredients, Long>, RecipeIngredientRepositoryCustom {
}
