package com.healthtracker.webapp.repository;

import com.healthtracker.webapp.domain.Recipe;
import com.healthtracker.webapp.repository.custom.RecipeRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Long>, RecipeRepositoryCustom {
}
