package com.healthtracker.webapp.repository.impl;

import com.healthtracker.webapp.repository.custom.IngredientRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class IngredientRepositoryImpl implements IngredientRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Integer getLatestId() {
        String query = "SELECT MAX(INGREDIENT_ID) FROM INGREDIENT";
        return (Integer) em.createNativeQuery(query).getSingleResult();
    }
}
