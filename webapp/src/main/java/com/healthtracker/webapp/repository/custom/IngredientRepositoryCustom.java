package com.healthtracker.webapp.repository.custom;

public interface IngredientRepositoryCustom {

    Integer getLatestId();
}
