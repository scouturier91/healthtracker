package com.healthtracker.webapp.repository;

import com.healthtracker.webapp.domain.UserFoodItem;
import com.healthtracker.webapp.repository.custom.UserFoodItemRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserFoodItemRepository extends JpaRepository<UserFoodItem, Long>, UserFoodItemRepositoryCustom {
}
