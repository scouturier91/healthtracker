package com.healthtracker.webapp.repository.impl;

import com.healthtracker.webapp.repository.custom.RecipeRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class RecipeRepositoryImpl implements RecipeRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Integer getLatestId() {
        String query = "SELECT MAX(RECIPE_ID) FROM RECIPE";

        return (Integer) em.createNativeQuery(query).getSingleResult();
    }
}
