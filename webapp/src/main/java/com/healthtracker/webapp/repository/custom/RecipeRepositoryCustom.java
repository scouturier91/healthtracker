package com.healthtracker.webapp.repository.custom;

public interface RecipeRepositoryCustom {
    Integer getLatestId();
}
