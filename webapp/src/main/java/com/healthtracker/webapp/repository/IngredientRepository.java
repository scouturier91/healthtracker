package com.healthtracker.webapp.repository;

import com.healthtracker.webapp.domain.Ingredient;
import com.healthtracker.webapp.repository.custom.IngredientRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IngredientRepository extends JpaRepository<Ingredient, Long>, IngredientRepositoryCustom {
}
